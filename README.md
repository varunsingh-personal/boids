# Boids
Demo [here](https://varunsingh-personal.gitlab.io/boids)
-----

Boids is an attempt to understand [Swarm Intelligence](https://en.wikipedia.org/wiki/Swarm_intelligence). The beauty of Swarm intelligence lies in ignorance and selfishness of an individual in a larger system. These attributes when combined together elicit a collective behaviour which cannot be predicted when looking at an individual. 
This particular algorithm mimics [flocking of birds](https://www.youtube.com/watch?v=V4f_1_r80RY) or [fish schooling](https://www.youtube.com/watch?v=Hg-NsZQFSAk).

[Craig Reynolds](https://www.red3d.com/cwr/boids/) designed a set of simple rules which when combined together, produce the flocking behaviour. This project is to understand and implement these rules.

Flocking works on three main rules: *Separation*, *Alignment*, *Cohesion*
Each individual or boid is only aware of its immediate surrounding. There is no central control. Each boid follows three simple rules:
 1. Separation: Stay away from your neighbours 
 2. Alignment: Look where your neighbours are looking
 3. Cohesion: Go in the general direction of your neighbours

The project spawns a bunch of boids on the screen with random direction and force. Soon they start reacting to their neighbours:

![Intro](https://media.giphy.com/media/Xxnx59q2DDG6dMd11f/giphy.gif)


There is an '**Analysis**' button with **🔍** icon. Upon clicking this, a boid gets chosen for analysis and you can see how is it following the three rules of flocking. Separation in Cyan, Alignment in White and Cohesion in Yellow coloured lines. The red circle around it is its radius of vision. It only reacts to other boids in its vision-radius:

![Analysis](https://media.giphy.com/media/rwxypRM42xWEwtCGUK/giphy.gif)


When  🔍 is clicked, three buttons become visible to toggle the rules.

**Separation**: The boids only care about keep away from each other:

![Separation](https://media.giphy.com/media/OaKUazUu4LeJIMYSJA/giphy.gif)


**Alignment**: The boids only care about keeping their heads in the same direction:

![Alignment](https://media.giphy.com/media/6DDw3oIayhsujSHz9R/giphy.gif)


**Cohesion**: The boids only care about following their neighbours:

![Cohesion](https://media.giphy.com/media/EMUwmv1SrQ2cFLNPK8/giphy.gif)


Boids also react to **Obstacles**. You can click anywhere on the screen to add an obstacle. Boids will try to avoid it:

![Obstacles](https://media.giphy.com/media/NAjWkX0hMk3OzNJrS4/giphy.gif)
