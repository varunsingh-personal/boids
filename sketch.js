/*jshint esversion: 6 */

let scene

let isMobile = false

let controls

class Controls {
  constructor() {
    this.sep = createButton("Separation")
    this.ali = createButton("Alignment")
    this.co = createButton("Cohesion")
    this.an = createButton('🔍')
    this.widget = createDiv('')
    this.bg = createDiv('')
    this.sepOn = true
    this.aliOn = true
    this.cohesOn = true
  }
  
  getSeparateButton() {
    return this.sep
  }
  
  getAlignButton() {
    return this.ali
  }
  
  getCohesionButton() {
    return this.co
  }
  
  getAnalysisButton() {
    return this.an
  }
  
  getWidgetButton() {
    return this.widget
  }
  
  getBgButton() {
    return this.bg
  }

  getSepOn() {
    return this.sepOn
  }

  getAliOn() {
    return this.aliOn
  }

  getCohesOn() {
    return this.cohesOn
  }

  setSepOn(sepOn) {
    this.sepOn = sepOn
  }

  setAliOn(aliOn) {
    this.aliOn = aliOn
  }

  setCohesOn(cohesOn) {
    this.cohesOn = cohesOn
  }
}

//Overall scene
class Scene {
  constructor(sceneWidth, sceneHeight) {
    this.sceneWidth = sceneWidth
    this.sceneHeight = sceneHeight
    this.boids = []
    this.seekCoeff = 1.0
    this.separationCoeff = 1.0
    this.alignmentCoeff = 1.0
    this.cohesionCoeff = 0.95
    this.numBoids = isMobile ? 100 : 325
    this.maxSpeed = Math.max(this.sceneWidth, this.sceneHeight) * 0.0026
    this.boidMass = 1.0
    this.maxForce = 0.075
    this.boidSize = 3
    this.obstacles = []
    this.backgroundColor = color(0, 0, 102)
    this.boidColor = color(255, 51, 0)
    this.obstacleColor = color(102, 0, 102)
    this.alignmentAnalysisColor = color('white')
    this.separationAnalysisColor = color('cyan')
    this.cohesionAnalysisColor = color('yellow')
    this.boidAnalysisColor = color('rgba(255, 0, 0, 0.25)')
    this.showAnalysis = false
  }

  getSeparationCoeff() {
    return this.separationCoeff;
  }

  getAlignmentCoeff() {
    return this.alignmentCoeff
  }

  getCohesionCoeff() {
    return this.cohesionCoeff
  }

  getNumBoids() {
    return this.numBoids
  }

  getMaxSpeed() {
    return this.maxSpeed
  }

  getBoidMass() {
    return this.boidMass
  }

  getMaxForce() {
    return this.maxForce
  }

  addBoid(b) {
    this.boids.push(b)
  }

  removeABoid() {
    this.boids.pop()
  }

  getBoid(index) {
    return this.boids[index]
  }

  getBoids() {
    return this.boids
  }

  getBoidSize() {
    return isMobile ? Math.max(this.sceneWidth, this.sceneHeight) * 0.005 : Math.max(this.sceneWidth, this.sceneHeight) * 0.0025
  }

  getSceneWidth() {
    return this.sceneWidth
  }

  getSceneHeight() {
    return this.sceneHeight
  }

  getObstacles() {
    return this.obstacles
  }

  setSeparationCoeff(separationCoeff) {
    this.separationCoeff = separationCoeff;
  }

  setAlignmentCoeff(alignmentCoeff) {
    this.alignmentCoeff = alignmentCoeff
  }

  setCohesionCoeff(cohesionCoeff) {
    this.cohesionCoeff = cohesionCoeff
  }

  setNumBoids(numBoids) {
    this.numBoids = numBoids
  }

  setMaxSpeed(maxSpeed) {
    this.maxSpeed = maxSpeed
  }

  setBoidMass(boidMass) {
    this.boidMass = boidMass
  }

  setMaxForce(maxForce) {
    this.maxForce = maxForce
  }

  setBoidSize(boidSize) {
    this.boidSize = boidSize
  }

  setSceneWidth(sceneWidth) {
    this.sceneWidth = sceneWidth
  }

  setSceneHeight(sceneHeight) {
    this.sceneHeight = sceneHeight
  }

  getBackgroundColor() {
    return this.backgroundColor
  }

  getAlignmentAnalysisColor() {
    return this.alignmentAnalysisColor
  }

  getSeparationAnalysisColor() {
    return this.separationAnalysisColor
  }

  getCohesionAnalysisColor() {
    return this.cohesionAnalysisColor
  }

  getBoidAnalysisColor() {
    return this.boidAnalysisColor
  }

  getBoidColor() {
    return this.boidColor
  }

  getObstacleColor() {
    return this.obstacleColor
  }

  getShowAnalysis() {
    return this.showAnalysis
  }

  setShowAnalysis(analysis) {
    this.showAnalysis = analysis
  }

  calcDistance(left, right) {
    return p5.Vector.dist(right.getPosition(), left.getPosition())
  }

  calculateVisibleBoids(boid) {
    boid.getVisibleBoids().length = 0
    for (let i = this.boids.length - 1; i >= 0; i--) {
      let cBoid = this.boids[i]
      if (cBoid.getId() === boid.getId()) {
        continue;
      }
      let distance = this.calcDistance(boid, cBoid)
      if (distance < cBoid.getVisionRadius()) {
        boid.getVisibleBoids().push(cBoid)
      }
    }
  }

  getCollidingObstacle(boid) {
    let retVal;
    for (let i = this.obstacles.length - 1; i >= 0; i--) {
      let obstacle = this.obstacles[i]
      let collisionDetector = boid.getCollisionDetector()
      let distance = abs(p5.Vector.dist(obstacle.position, collisionDetector))
      let diff = obstacle.size/2 - distance
      if (diff > 0) {
        let o2b = p5.Vector.sub(obstacle.getPosition(), boid.getPosition())
        let cd2b = p5.Vector.sub(boid.getCollisionDetector(), boid.getPosition())
        retVal = new ObstacleInfo(obstacle, distance, o2b.angleBetween(cd2b))
        return retVal
      }
      let boidPosition = boid.getPosition()
      distance = abs(p5.Vector.dist(obstacle.position, boidPosition))
      diff = obstacle.size/2 - distance
      if (diff > 0) {
        let o2b = p5.Vector.sub(obstacle.getPosition(), boid.getPosition())
        let cd2b = p5.Vector.sub(boid.getCollisionDetector(), boid.getPosition())
        retVal = new ObstacleInfo(obstacle, distance, o2b.angleBetween(cd2b))
        return retVal
      }
    }
  }

  addObstacle(x, y) {
    this.obstacles.length = 0
    this.obstacles.push(new Obstacle(createVector(x, y), random(isMobile ? this.getSceneWidth() / 10 : this.getSceneWidth() / 15, isMobile ? this.getSceneWidth() / 5 : this.getSceneWidth() / 10)))
  }

  renderObstacles() {
    for (var i = this.obstacles.length - 1; i >= 0; i--) {
      let o = this.obstacles[i]
      push()
      stroke(this.obstacleColor)
      fill(this.obstacleColor)
      circle(o.position.x, o.position.y, o.size)
      pop()
    }
  }
}

class Obstacle {
  constructor(position, size) {
    this.position = position
    this.size = size
  }

  getPosition() {
    return this.position
  }

  getSize() {
    return this.size
  }
}

class ObstacleInfo {
  constructor(obstacle, distance, angle) {
    this.obstacle = obstacle
    this.distance = distance
    this.angle = angle
  }

  getObstacle() {
    return this.obstacle
  }

  getDistance() {
    return this.distance
  }

  getAngle() {
    return this.angle
  }
}

//Represents a Boid
class Boid {
  constructor(id, position, velocity, size, scene) {
    this.scene = scene
    this.id = id
    this.position = position
    this.velocity = velocity
    this.acceleration = createVector(0,0)
    this.x = position.x
    this.y = position.y
    this.mass = scene.getBoidMass()
    this.maxSpeed = scene.getMaxSpeed()
    this.maxForce = scene.getMaxForce()
    this.size = size
    this.orientation = 0
    this.visionRadius = size * 7.5
    this.color = scene.getBoidColor()
    this.visibleBoids = []
    this.collisionDetector = createVector(0, 0)
    this.separationSteer = createVector(0, 0)
    this.separationPosition = createVector(0, 0)
    this.alignmentSteer = createVector(0, 0)
    this.alignmentPosition = createVector(0, 0)
    this.cohesionSteer = createVector(0, 0)
    this.cohesionPosition = createVector(0, 0)
    this.avoidCollisionSteer = createVector(0, 0)
    this.analyzed = false
  }

  getId() {
    return this.id
  }

  getPosition() {
    return this.position
  }

  getVelocity() {
    return this.velocity
  }

  setColor(color) {
    this.color = color
  }

  getVisionRadius() {
    return this.visionRadius
  }

  getCollisionDetector() {
    return this.collisionDetector
  }

  getVisibleBoids() {
    return this.visibleBoids
  }

  isAnalyzed() {
    return this.analyzed
  }

  setAnalyzed(analyzed) {
    this.analyzed = analyzed
  }

  refreshMaxParams() {
    this.maxForce = scene.getMaxForce()
    this.maxSpeed = scene.getMaxSpeed()
  }
  
  //updates the position of Boid based in window's width and height
  updatePosition() {
    let wWidth = scene.getSceneWidth()
    let wHeight = scene.getSceneHeight()

    this.velocity.add(this.acceleration).limit(this.maxSpeed)
    this.position.add(this.velocity)
    this.x = this.position.x
    this.y = this.position.y
    
    if(this.x <= 0) {
      this.position.x = wWidth
    }
    
    if(this.x >= wWidth) {
      this.position.x = 0
    }
    
    if(this.y <= 0) {
      this.position.y = wHeight
    }
    
    if (this.y >= wHeight) {
      this.position.y = 0
    }

    this.collisionDetector.set(this.position.x + (this.velocity.x * 20), this.position.y + (this.velocity.y * 20))
    this.orientation = this.velocity.heading() + degrees(radians(90))
  }
  
  //Draws the shape of boid at calculated position
  draw() {
    if (this.analyzed) {
      push()
      // line(this.position.x, this.position.y, this.position.x + (this.velocity.x * 10), this.position.y + (this.velocity.y * 10))
      strokeWeight(5)

      if (controls.getSepOn()) {
        stroke(scene.getSeparationAnalysisColor())
        line(this.position.x, this.position.y, this.position.x + this.separationPosition.x * 350, this.position.y + this.separationPosition.y * 350)
      }

      if (controls.getAliOn()) {
        stroke(scene.getAlignmentAnalysisColor())
        line(this.position.x, this.position.y, this.position.x + (this.alignmentPosition.x * 10), this.position.y + (this.alignmentPosition.y * 10))
      }

      if (controls.getCohesOn()) {
        stroke(scene.getCohesionAnalysisColor())
        line(this.position.x, this.position.y, this.position.x + this.cohesionPosition.x * 350, this.position.y + this.cohesionPosition.y * 350)
      }

      pop()

      push()
      if (isMobile) {
        fill('rgba(255, 0, 0, 0)')
      } else {
        fill(scene.getBoidAnalysisColor())
      }
      stroke('white')
      circle(this.position.x, this.position.y, this.visionRadius)
      pop()
      stroke('pink')
      fill('pink')

    } else {
      fill(this.color)
      stroke(this.color)
    }
    push()
    translate(this.position.x, this.position.y)
    rotate(this.orientation)
    triangle(         0, -this.size * 2,
             -this.size,  this.size * 2, 
              this.size,  this.size * 2)
    pop()
  }

  drawId() {
    push()
    textSize(15)
    text(this.getId(), this.x, this.y)
    pop()
  }

  calculateVisibleBoids() {
    this.scene.calculateVisibleBoids(this)
  }


  resetAcceleration() {
    this.acceleration.mult(0)
  }
  
  //Moves the Boid with given velocity
  move(velocity) {
    this.velocity.add(velocity).mult(maxSpeed)
  }
  
  //Seeks the target with rigour
  seekNormal(target) {
    this.velocity = p5.Vector.sub(target, this.position).normalize().mult(this.maxSpeed)
  }
  
  //Calculates the seeking steer
  getSeekSteer(target) {
    let desiredVelocity = p5.Vector.sub(target, this.position).normalize().mult(this.maxSpeed)
    
    let steering = p5.Vector.sub(desiredVelocity, this.velocity).limit(this.maxForce)
    
    return steering
    
  }
  
  //Seeks the target naturally
  seek(target) {
    let steer = this.getSeekSteer(target)
    
    this.acceleration.add(steer)
  }
  
  //Flees from targer
  flee(target) {
    
    let desiredVelocity = p5.Vector.sub(this.position, target).normalize().mult(this.maxSpeed)
    
    let steering = p5.Vector.sub(desiredVelocity, this.velocity).limit(this.maxForce)
    
    this.acceleration.add(steering)
  }
  
  //Arrives at target after seeking it. Radius tells when to start slowing down
  seekAndArrive(target) {
    let steer = this.getSeekSteer(target)
    
    let targetOffset = p5.Vector.sub(target, this.position)
    let distance = targetOffset.mag()
    let rampedSpeed = this.maxSpeed;
    if(distance <= this.visionRadius) {
      rampedSpeed = this.maxSpeed * (distance / this.visionRadius)
      let clipped_speed = min(rampedSpeed, this.maxSpeed)
      let desiredVelocity = targetOffset.mult(clipped_speed / distance)
      steer = p5.Vector.sub(desiredVelocity, this.velocity)
    }
    this.acceleration.add(steer)

  }

  separate() {
    this.separationSteer.set(0, 0)
    this.separationPosition.set(0, 0)
    for (var i = this.visibleBoids.length - 1; i >= 0; i--) {
      let vBoid = this.visibleBoids[i]
      let dist = p5.Vector.dist(this.position, vBoid.getPosition())

      let repulsiveForce = p5.Vector.sub(this.getPosition(), vBoid.getPosition()).normalize().div(dist)
      this.separationSteer.add(repulsiveForce)
      this.separationPosition = this.separationSteer
    }

    if (this.visibleBoids.length) {
      this.separationSteer.div(this.visibleBoids.length)
    }
    this.separationPosition = this.separationSteer

    if (this.separationSteer.mag() > 0) {
      this.separationSteer.normalize().mult(this.maxSpeed).sub(this.velocity).limit(this.maxForce)
    }

    this.separationSteer.mult(scene.getSeparationCoeff())
    this.acceleration.add(this.separationSteer)
    
  }

  align() {
    this.alignmentSteer.set(0, 0)
    this.alignmentPosition.set(0, 0)
    for (var i = this.visibleBoids.length - 1; i >= 0; i--) {
      let vBoid = this.visibleBoids[i]
      this.alignmentSteer.add(vBoid.getVelocity())
    }
    if (this.visibleBoids.length) {
      if (this.isAnalyzed) {
        this.alignmentPosition = p5.Vector.div(this.alignmentSteer, this.visibleBoids.length)
      }
      
      this.alignmentSteer.div(this.visibleBoids.length).normalize().mult(this.maxSpeed)
      if (this.alignmentSteer.mag() > 0) {
        this.alignmentSteer.sub(this.velocity).limit(this.maxForce)
      }
      this.alignmentSteer.mult(scene.getAlignmentCoeff())
    } else {
      this.alignmentSteer.set(0, 0)
    }

    this.acceleration.add(this.alignmentSteer)
    
  }

  cohesion() {
    this.cohesionSteer.set(0, 0)
    this.cohesionPosition.set(0, 0)
    for (var i = this.visibleBoids.length - 1; i >= 0; i--) {
      let vBoid = this.visibleBoids[i]
      this.cohesionSteer.add(vBoid.getPosition())
    }

    if (this.visibleBoids.length) {
      if (this.isAnalyzed) {
        this.cohesionPosition = p5.Vector.div(this.cohesionSteer, this.visibleBoids.length)
      }
      this.cohesionSteer.div(this.visibleBoids.length)
      this.cohesionSteer = this.getSeekSteer(this.cohesionSteer)
      
    } else {
      this.cohesionSteer.set(0, 0)
    }
    this.cohesionSteer.mult(scene.getCohesionCoeff())

    if (this.isAnalyzed) {
        this.cohesionPosition = this.cohesionSteer
      }
    this.acceleration.add(this.cohesionSteer)
    
  }

  avoidCollision() {
    let obsInfo = scene.getCollidingObstacle(this)
    if (obsInfo) {
      this.avoidCollisionSteer = p5.Vector.sub(this.collisionDetector, obsInfo
        .getObstacle()
        .getPosition())
      .div(obsInfo.getDistance())
      .normalize()
    }

    this.acceleration.add(this.avoidCollisionSteer)
    this.avoidCollisionSteer.set(0, 0)

  }

  renderVector() {
    push()
    line(this.x, this.y, this.x + this.velocity.x, this.y + this.velocity.y)
    pop()
  }
  
  render() {
    this.refreshMaxParams()
    this.updatePosition()
    this.draw()
    this.calculateVisibleBoids()
    this.resetAcceleration()
  }
}

p5.disableFriendlyErrors = true

function setupUi() {
  controls = new Controls()
  let bg = controls.getBgButton()
  bg.position(0, 0)
  if (isMobile) {
    bg.size(windowWidth, 200)
  } else {
    bg.size(windowWidth, 50)
  }
  
  bg.style('background', 'rgba(120, 120, 120, 0.5)')

  let widget = controls.getWidgetButton()
  widget.size(bg.size().width * 0.75, bg.size().height)
  widget.parent(bg)
  widget.center('horizontal')
  
  let w = widget.size().width / 3.25
  let h = widget.size().height / 1.5
  let x = 0
  let y = widget.size().height / 2 - h / 2
  let p = w + w / 8.3
  
  let sep = controls.getSeparateButton()
  sep.parent(widget)
  sep.size(w, h)
  sep.position(x, y)
  
  if (isMobile) {
    sep.addClass('sepMobileOn')
  } else {
    sep.addClass('sepOn')
  }

  x += sep.position().x
  sep.mousePressed(sepPressed)
  
  let ali = controls.getAlignButton()
  ali.parent(widget)
  ali.size(w, h)
  ali.position(x + p, y)
  if (isMobile) {
    ali.addClass('alignMobileOn')
  } else {
    ali.addClass('alignOn')
  }
  ali.mousePressed(aliPressed)
  x += ali.position().x
  
  let co = controls.getCohesionButton()
  co.parent(widget)
  co.size(w, h)
  co.position(x + p, y)
  
  if (isMobile) {
    co.addClass('cohesMobileOn')
  } else {
    co.addClass('cohesOn')
  }
  co.mousePressed(coPressed)
  
  let an = controls.getAnalysisButton()
  let s
  if (isMobile) {
    s = min(100, max(windowWidth, windowHeight) / 20)
  } else {
    s = min(50, max(windowWidth, windowHeight) / 20)
  }
  
  an.size(s, s)
  an.position(windowWidth - s - 10, windowHeight - s - 10)
  
  if (isMobile) {
    an.addClass('anMobileOn')
  } else {
    an.addClass('anOn')
  }
  an.mousePressed(anPressed)
  
  bg.hide()
}

function setup() {
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      isMobile = true
  }

  if (isMobile) {
    pixelDensity(0.5)
  }
  let wWidth = windowWidth
  let wHeight = windowHeight
  createCanvas(wWidth, wHeight);  
  scene = new Scene(wWidth, wHeight)

  for(let i = 0; i < scene.getNumBoids(); i++) {
    scene.addBoid(
      new Boid(
      i, 
      createVector(random(wWidth / 4, wWidth - wWidth / 4), random(wHeight / 4, wHeight - wHeight / 4)), 
      // createVector(wWidth / 2, wHeight / 2),
      createVector(random(-1,1), random(-1,1)), 
      scene.getBoidSize(),
      scene))
  }
  angleMode(DEGREES)
  rectMode(CENTER)
  // noLoop()
  setupUi()

}

function draw() {
  background(scene.getBackgroundColor())
    
  for(let i = 0; i < scene.getNumBoids(); i++) {
    let cBoid = scene.getBoid(i)
    cBoid.render(scene.getSceneWidth(), scene.getSceneHeight())
    cBoid.separate()
    cBoid.align()
    cBoid.cohesion()
    cBoid.avoidCollision()
  }

  scene.renderObstacles()  
}

function windowResized() {
  scene.setSceneWidth(windowWidth)
  scene.setSceneHeight(windowHeight)
  resizeCanvas(windowWidth, windowHeight)
}

function mouseClicked(event) {
  let cx = controls.getBgButton().position().x
  let cy = controls.getBgButton().position().y
  let cw = controls.getBgButton().size().width
  let ch = controls.getBgButton().size().height

  let ax = controls.getAnalysisButton().position().x
  let ay = controls.getAnalysisButton().position().y
  let aw = controls.getAnalysisButton().size().width
  let ah = controls.getAnalysisButton().size().height

  let x = event.x
  let y = event.y
  if (scene.getShowAnalysis()) {
    if (x >= cx && x <= cx + cw && y >= cy && y <= cy + ch) {
      return 
    }
  }

  if (x >= ax && x <= ax + aw && y >= ay && y <= ay + ah) {
      return 
    }

  scene.addObstacle(event.x, event.y)
}

function sepPressed() {
  let sep = controls.getSeparateButton()
  if (isMobile) {
    if (sep.class() === 'sepMobileOn') {
        sep.removeClass('sepMobileOn')
        sep.addClass('sepMobileOff')
        scene.setSeparationCoeff(0.0)
        controls.setSepOn(false)
      } else {
        sep.removeClass('sepMobileOff')
        sep.addClass('sepMobileOn')
        scene.setSeparationCoeff(1.0)
        controls.setSepOn(true)
      }
  } else {
    if (sep.class() === 'sepOn') {
      sep.removeClass('sepOn')
      sep.addClass('sepOff')
      scene.setSeparationCoeff(0.0)
      controls.setSepOn(false)
    } else {
      sep.removeClass('sepOff')
      sep.addClass('sepOn')
      scene.setSeparationCoeff(1.0)
      controls.setSepOn(true)
    }
  }
  
}

function aliPressed() {
  let ali = controls.getAlignButton()

  if (isMobile) {
    if (ali.class() === 'alignMobileOn') {
        ali.removeClass('alignMobileOn')
        ali.addClass('alignMobileOff')
        scene.setAlignmentCoeff(0.0)
        controls.setAliOn(false)
      } else {
        ali.removeClass('alignMobileOff')
        ali.addClass('alignMobileOn')
        scene.setAlignmentCoeff(1.0)
        controls.setAliOn(true)
      }
  } else {
    if (ali.class() === 'alignOn') {
      ali.removeClass('alignOn')
      ali.addClass('alignOff')
      scene.setAlignmentCoeff(0.0)
      controls.setAliOn(false)
    } else {
      ali.removeClass('alignOff')
      ali.addClass('alignOn')
      scene.setAlignmentCoeff(1.0)
      controls.setAliOn(true)
    }
  }
}

function coPressed() {
  let co = controls.getCohesionButton()
  if (isMobile) {
    if (co.class() === 'cohesMobileOn') {
        co.removeClass('cohesMobileOn')
        co.addClass('cohesMobileOff')
        scene.setCohesionCoeff(0.0)
        controls.setCohesOn(false)
      } else {
        co.removeClass('cohesMobileOff')
        co.addClass('cohesMobileOn')
        scene.setCohesionCoeff(0.95)
        controls.setCohesOn(true)
      }
  } else {
    if (co.class() === 'cohesOn') {
      co.removeClass('cohesOn')
      co.addClass('cohesOff')
      scene.setCohesionCoeff(0.0)
      controls.setCohesOn(false)
    } else {
      co.removeClass('cohesOff')
      co.addClass('cohesOn')
      scene.setCohesionCoeff(0.95)
      controls.setCohesOn(true)
    }
  }
  
}

function anPressed() {
  let bg = controls.getBgButton()
  let an = controls.getAnalysisButton()

  if (isMobile) {
    if (an.class() === 'anMobileOn') {
      an.removeClass('anMobileOn')
      an.addClass('anMobileOff')
      bg.show()
      scene.getBoids()[0].setAnalyzed(true)
      scene.setShowAnalysis(true)
    } else {
      an.removeClass('anMobileOff')
      an.addClass('anMobileOn')
      bg.hide()
      scene.getBoids()[0].setAnalyzed(false)
      scene.setShowAnalysis(false)
    }
  } else {
    if (an.class() === 'anOn') {
      an.removeClass('anOn')
      an.addClass('anOff')
      bg.show()
      scene.getBoids()[0].setAnalyzed(true)
      scene.setShowAnalysis(true)
    } else {
      an.removeClass('anOff')
      an.addClass('anOn')
      bg.hide()
      scene.getBoids()[0].setAnalyzed(false)
      scene.setShowAnalysis(false)
    }
  }
  
}

